-module(hofs).
-export([doubleAll/1, evens/1, product/1,
    zip/2,
    zip_with/3,
    zip_withHof/3,
    zip2/2]).

% eunit:test(hofs).
-include_lib("eunit/include/eunit.hrl").

-spec doubleAll([number()]) -> [number()].
doubleAll(Xs) -> lists:map(fun(X) -> X*2 end, Xs).

doubleAll_test() ->
?assertEqual([],doubleAll([])),
?assertEqual([2,4],doubleAll([1,2])).


-spec evens([number()]) -> [number()].
evens(Xs) -> lists:filter(fun(X) -> X rem 2 == 0 end, Xs).

evens_test() ->
?assertEqual([],evens([])),
?assertEqual([2,4],evens([1,2,3,4,5])).


-spec product([number()]) -> number().
product(Xs) -> lists:foldr(fun(X,Acc) -> Acc * X end, 1, Xs).

product_test() ->
?assertEqual(1,product([])),
?assertEqual(24,product([1,2,3,4])).


-spec zip([T1],[T2]) -> [{T1,T2}].
zip([],_) -> [];
zip(_,[]) -> [];
zip([X|Xs],[Y|Ys]) -> [{X,Y}|zip(Xs,Ys)].

zip_test() ->
?assertEqual([],zip([],[])),
?assertEqual([ {1,2}, {3,4} ],zip([1,3,5,7], [2,4])).


-spec zip_with(fun((T1,T2) -> T3),[T1],[T2]) -> [T3].
zip_with(_,[],_) -> [];
zip_with(_,_,[]) -> [];
zip_with(F,[X|Xs],[Y|Ys]) -> [F(X,Y) | zip_with(F,Xs,Ys)].

zip_with_test() ->
?assertEqual([],zip_with(nil,[],[])),
?assertEqual([4,4,4],zip_with(fun(X,Y) -> X+Y end, [1,2,3], [3,2,1])).


-spec zip_withHof(fun((T1,T2) -> T3),[T1],[T2]) -> [T3].
zip_withHof(F,Xs,Ys) -> lists:map(fun({X,Y}) -> F(X,Y) end, zip(Xs,Ys)).

zip_withHof_test() ->
?assertEqual([],zip_withHof(nil,[],[])),
?assertEqual([4,4,4],zip_withHof(fun(X,Y) -> X+Y end, [1,2,3], [3,2,1])).


-spec zip2([T1],[T2]) -> [{T1,T2}].
zip2(Xs,Ys) -> zip_with(fun(X,Y) -> {X,Y} end,Xs,Ys).

zip2_test() ->
?assertEqual([],zip2([],[])),
?assertEqual([ {1,2}, {3,4} ],zip2([1,3,5,7], [2,4])).
