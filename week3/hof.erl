-module(hof).
-export([add/1,times/1,compose/2,id/1,iterate/1,
        list_compose/1]).

% eunit:test(hof).
-include_lib("eunit/include/eunit.hrl").


add(X) ->
    fun(Y) -> X+Y end.

times(X) ->
    fun(Y) ->
	     X*Y end.

compose(F,G) ->
    fun(X) -> G(F(X)) end.

id(X) ->
    X.
      
% ###################
% mitt jox följer
% ###################

% om man ska göra en rekursiv definition
% av funktion så går det med denna syntax.
%Foo = fun Product([]) -> 1 ; Product([X|Xs]) -> X*Product(Xs) end.

% Composition is associative – meaning that it doesn’t matter how you bracket a composition of three functions – but it’s not commutative – so that F composed with G is not necessarily the same as G composed with F.
% Find examples of F and G to show cases when the two compositions are the same, and when they are different.

composeNotCommutative_test() ->
?assertEqual(9,(hof:compose(hof:add(2),hof:times(3)))(1)),
?assertEqual(5,(hof:compose(hof:times(3),hof:add(2)))(1)).

% Define a function that takes a list of functions and composes them together. Can you use any of the functions in the lists module to help you?
-spec list_compose([fun()]) -> fun().
list_compose(Fs) ->
    fun(X) -> lists:foldl(fun(F,Acc) -> F(Acc) end ,X ,Fs) end.

list_compose_test() ->
?assertEqual(9,(list_compose([add(2),times(3)]))(1)),
?assertEqual(5,(list_compose([times(3),add(2)]))(1)).

-spec twice(fun()) -> fun().
twice(F) -> compose(F,F).

twice_test() ->
?assertEqual(18,(twice(times(3)))(2)),

% What happens when you apply twice to itself?
% twice of twice means new function for applying 4 times, ie:
?assertEqual(4,(twice(times(2)))(1)),
?assertEqual(16,(twice(twice(times(2))))(1)),

% What happens when you apply the result of that to “multiply by 3” and the result of that to 2?
% ( 2 ( * 3 * 3 ) * 3 * 3 = 2 * 9 * 9 = 162 
?assertEqual(162,(twice(twice(times(3))))(2)).



% Define a function iterate that takes a number N and returns
% a function that takes a function and returns
% that function iterated N times.
% When N is zero, it should return the identity function (that is, the function that returns its argument unchanged).

iterate(N) ->
    fun (F) ->
        FunList = lists:map(fun(_) -> F end, lists:seq(1,N)),
        list_compose(FunList) end.

iterate_test() ->
FunTwiceTimes2 = (iterate(2))(times(2)),
Fun3TimesTimes2 = (iterate(3))(times(2)),
Fun0TimesTimes2 = (iterate(0))(times(2)),
?assertEqual(4,FunTwiceTimes2(1)),
?assertEqual(8,Fun3TimesTimes2(1)),
?assertEqual(a,Fun0TimesTimes2(a)).

