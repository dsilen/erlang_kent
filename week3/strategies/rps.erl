-module(rps).
-export([play/1,echo/1,play_two/3,rock/1,no_repeat/1,
    enum/1,cycle/1,rand/1,val/1,tournament/2,
    least/1,
    most/1,
    randomFrom/1,
    bestPerformer/1,
    history/1]).

-include_lib("eunit/include/eunit.hrl").


%
% interactively play against a strategy, provided as argument.
%

play(Strategy) ->
    io:format("Rock - paper - scissors~n"),
    io:format("Play one of rock, paper, scissors, ...~n"),
    io:format("... r, p, s, stop, followed by '.'~n"),
    play(Strategy,[]).

% tail recursive loop for play/1

play(Strategy,Moves) ->
    {ok,P} = io:read("Play: "),
    Play = expand(P),
    case Play of
	stop ->
	    io:format("Stopped~n");
	_    ->
	    Result = result(Play,Strategy(Moves)),
	    io:format("Result: ~p~n",[Result]),
	    play(Strategy,[Play|Moves])
    end.

%
% auxiliary functions
%

% transform shorthand atoms to expanded form
    
expand(r) -> rock;
expand(p) -> paper;		    
expand(s) -> scissors;
expand(X) -> X.

% result of one set of plays

result(rock,rock) -> draw;
result(rock,paper) -> lose;
result(rock,scissors) -> win;
result(paper,rock) -> win;
result(paper,paper) -> draw;
result(paper,scissors) -> lose;
result(scissors,rock) -> lose;
result(scissors,paper) -> win;
result(scissors,scissors) -> draw.

% result of a tournament

tournament(PlaysL,PlaysR) ->
    lists:sum(
      lists:map(fun outcome/1,
		lists:zipwith(fun result/2,PlaysL,PlaysR))).

outcome(win)  ->  1;
outcome(lose) -> -1;
outcome(draw) ->  0.

% transform 0, 1, 2 to rock, paper, scissors and vice versa.

enum(0) ->
    rock;
enum(1) ->
    paper;
enum(2) ->
    scissors.

val(rock) ->
    0;
val(paper) ->
    1;
val(scissors) ->
    2.

% give the play which the argument beats.

beats(rock) ->
    scissors;
beats(paper) ->
    rock;
beats(scissors) ->
    paper.

%
% strategies.
%
echo([]) ->
     paper;
echo([Last|_]) ->
    Last.

rock(_) ->
    rock.


% ################################
% FOR YOU TO DEFINE
% REPLACE THE dummy DEFINITIONS
% ################################

%loses(X) -> beats(beats(X)).

%
no_repeat([]) ->
    rock;
no_repeat([X|_]) ->
    beats(X).

no_repeat_test() ->
?assertEqual(scissors, no_repeat([rock])),
?assertEqual(rock, no_repeat([])).


%
rand(_) ->
   enum(rand:uniform(3)-1).

%
cycle(Xs) ->
   enum(length(Xs) rem 3).

cycle_test() ->
?assertEqual(cycle([a,b,c]), cycle([])).


%
-spec count([any()],map()) -> map().
count(Xs,Initial) ->
    IncrementX = fun(X,M) ->
        Count = case M of
            #{X:=C} -> C;
            _ -> 0 end,
        M#{X=>Count+1} end,
   lists:foldl(IncrementX,Initial,Xs).

emptyCounters() -> #{rock => 0, paper => 0, scissors => 0}.

%
least(P) -> {X,_} = hd(lists:keysort(2,maps:to_list(count(P,emptyCounters())))),
    X.


%
most(P) -> {X,_} = hd(lists:reverse(lists:keysort(2,maps:to_list(count(P,#{}))))),
    X.

%
-spec randomFrom([fun()]) -> fun().
randomFrom(Ss) ->
    Strategy = lists:nth(rand:uniform(length(Ss)), Ss),
    Strategy.




% Define a strategy that takes a list of strategies and each play
% chooses from the list the strategy which gets the best result
% when played against the list of plays made so far.

-spec bestPerformer([fun()]) -> fun().
bestPerformer(Ss) ->
    fun(Ps) ->
        History = history(Ps),
% collect statistics per strategy
% for each strategy
        Performances = lists:map(fun(Strategy) ->
% find the summed outcomes per history
            {Strategy, summed_outcomes(Strategy, History)}
        end, Ss),
% Sort and return the winner strategy
        {WinnerStrategy,_Points} = hd(lists:reverse(lists:keysort(2,Performances))),
        WinnerStrategy
    end.

% summed outcomes
summed_outcomes(Strategy,Hs) ->
    lists:foldl(fun({Head,Tail},Acc) ->
        outcome(result(Strategy(Tail),Head)) + Acc
    end, 0, Hs).

% history of plays as a list of heads and tail tuples.
history([]) -> [];
history([_]) -> [];
history([Head,Tail|Hs]) -> [{Head,[Tail|Hs]} | history([Tail|Hs])].




% Strategy vs Strategy
% Define a function that takes three arguments: two strategies and a number N, and which plays the strategies against each other for N turns. At each stage the function should output the result of the round, and it should show the result of the tournament at the end.
% You could also choose to modify this so that the game is ended when one player is more than M points ahead of the other, for example.

%
% play one strategy against another, for N moves.
%

play_two(StrategyL,StrategyR,N) ->
    play_two(StrategyL,StrategyR,[],[],N).


play_two(_,_,PlaysL,PlaysR,0) ->
    Sum = lists:foldl(fun({L,R},A) -> A+outcome(result(L,R)) end,0,lists:zip(PlaysL,PlaysR)),
    io:format("~s~n",[case Sum < 0 of true -> "Left wins."; _ ->
        case Sum > 0 of true -> "Right wins."; _ -> "draw" end
     end ]),
    {PlaysL,PlaysR};

play_two(StrategyL,StrategyR,PlaysL,PlaysR,N) when N > 0->
   Pl = StrategyL(PlaysR),
   Pr = StrategyR(PlaysL),
   io:format("~p ~p : ~s.~n",[Pl,Pr,case outcome(result(Pl,Pr)) of -1 -> "left wins"; 0 -> "draw"; 1 -> "right wins" end]),
   play_two(StrategyL,StrategyR,[Pl|PlaysL],[Pr|PlaysR],N-1).
