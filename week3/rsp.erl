-module(rsp).
-export([beat/1,
    lose/1,
    result/2,
    tournament/2]).

% eunit:test(rsp).
% typer rsp.erl
-include_lib("eunit/include/eunit.hrl").

-spec beat('paper'|'rock'|'scissor') -> 'paper'|'rock'|'scissor'.
beat(rock) -> paper;
beat(paper) -> scissor;
beat(scissor) -> rock.

beat_test() ->
?assertEqual(rock, rsp:beat(rsp:beat(rsp:beat(rock)))),
?assertEqual(scissor, rsp:beat(rsp:beat(rsp:beat(scissor)))),
?assertEqual(paper, rsp:beat(rsp:beat(rsp:beat(paper)))).


-spec lose('paper'|'rock'|'scissor') -> 'paper'|'rock'|'scissor'.
lose(rock) -> scissor;
lose(scissor) -> paper;
lose(paper) -> rock.

lose_test() ->
?assertEqual(rock, rsp:lose(rsp:lose(rsp:lose(rock)))),
?assertEqual(scissor, rsp:lose(rsp:lose(rsp:lose(scissor)))),
?assertEqual(paper, rsp:lose(rsp:lose(rsp:lose(paper)))).


-spec result('paper'|'rock'|'scissor','paper'|'rock'|'scissor') -> 'draw'|'lose'|'win'.
result(X,X) -> draw;
result(X,Y) -> case lose(X) == Y of
    true -> win;
    false -> lose end.

result_test() ->
?assertEqual(win, result(rock,scissor)),
?assertEqual(lose, result(rock,paper)),
?assertEqual(draw, result(rock,rock)).


outcome(win) -> 1;
outcome(lose) -> -1;
outcome(draw) -> 0.


-spec tournament(['paper'|'rock'|'scissor'],['paper'|'rock'|'scissor']) -> integer().
tournament(Ls,Rs) ->
    lists:foldl(fun(R, Sum) -> outcome(R) + Sum end,0,
        lists:zipwith(fun result/2, Ls, Rs)).


tournament_test() ->
?assertEqual(0, tournament([rock,rock,rock],[scissor,paper,rock])).
