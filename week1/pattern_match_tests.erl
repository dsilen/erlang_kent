-module(pattern_match_tests).
-include_lib("eunit/include/eunit.hrl").

xor1_test() ->
false = pattern_match:xor1(false,false),
true = pattern_match:xor1(true,false),
true = pattern_match:xor1(false,true),
false = pattern_match:xor1(true,true).

xor2_test() ->
false = pattern_match:xor2(false,false),
true = pattern_match:xor2(true,false),
true = pattern_match:xor2(false,true),
false = pattern_match:xor2(true,true).

xor3_test() ->
false = pattern_match:xor3(false,false),
true = pattern_match:xor3(true,false),
true = pattern_match:xor3(false,true),
false = pattern_match:xor3(true,true).

maxThree_test() ->
3 = pattern_match:maxThree(1,2,3),
3 = pattern_match:maxThree(1,3,2),
3 = pattern_match:maxThree(3,2,1).

howManyEqual_test() ->
3 = pattern_match:howManyEqual(a,a,a),
2 = pattern_match:howManyEqual(a,a,b),
2 = pattern_match:howManyEqual(a,b,a),
2 = pattern_match:howManyEqual(b,a,a),
0 = pattern_match:howManyEqual(a,b,c).
% eunit:test(first).
