-module(recur).
-export([fac/1,
    fib/1,
    pieces/1,
    tfib/1,
    perfect/1]).

fac(0) -> 1;
fac(N) when N > 0 -> N * fac(N-1).

fib(0) -> 0;
fib(1) -> 1;
fib(N) when N > 1 -> fib(N-1)+fib(N-2).

pieces(1) -> 2;
pieces(N) -> N + pieces(N-1).

tfib(N) when N > 0 ->
    tfib(N,0,1).
tfib(1,A1,A2) -> A1+A2;
tfib(N,A1,A2) -> tfib(N-1,A2,A1+A2).

%tfib(4) -> tfib(4,0,1)
%tfib(4) -> tfib(3,1,1)
%tfib(4) -> tfib(2,1,2)
%tfib(4) -> tfib(1,2,3)
%tfib(4) -> 2+3

perfect(N) when N > 0 ->
    perfect(N,N div 2,0).
perfect(N,0,A) -> A == N;
perfect(N,D,A) when (N rem D) == 0 ->
    perfect(N,D-1,A+D);
perfect(N,D,A) ->
    perfect(N,D-1,A).
