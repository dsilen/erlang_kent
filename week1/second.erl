-module(second).
-export([hypothenuse/2,perimeter/2,area/2]).

hypothenuse(X,Y) -> 
    SqX = first:square(X),
    SqY = first:square(Y),
    math:sqrt(SqX+SqY).

perimeter(X,Y) ->
    Z = hypothenuse(X,Y),
    X+Y+Z.

area(X,Y) ->
    first:mult(0.5,first:mult(X,Y)).
