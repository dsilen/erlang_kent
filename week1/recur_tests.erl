-module(recur_tests).
%-include(recur).
-include_lib("eunit/include/eunit.hrl").

fac_test() ->
1 = recur:fac(0),
1 = recur:fac(1),
2 = recur:fac(2),
6 = recur:fac(3).

fib_test() ->
0 = recur:fib(0),
1 = recur:fib(1),
1 = recur:fib(2),
2 = recur:fib(3),
3 = recur:fib(4).

pieces_test() ->
2 = recur:pieces(1),
4 = recur:pieces(2),
7 = recur:pieces(3),
11 = recur:pieces(4).


% fib(4) = fib(3)+fib(2)
% fib(4) = fib(2)+1+1+0
% fib(4) = 1+0+1+1+0
% eunit:test(recur_tests).