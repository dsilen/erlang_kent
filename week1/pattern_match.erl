-module(pattern_match).
-export([xor1/2,xor2/2,xor3/2,
    maxThree/3,
    howManyEqual/3]).

xor1(X,Y) ->
    X =/= Y.

xor2(X,Y) ->
    (X and not Y) or (not X and Y).

xor3(false,Y) -> Y;
xor3(true,Y) -> not Y.

maxThree(X,Y,Z) ->
    max(max(X,Y),Z).

howManyEqual(X,X,X) -> 3;
howManyEqual(X,X,_) -> 2;
howManyEqual(X,_,X) -> 2;
howManyEqual(_,X,X) -> 2;
howManyEqual(_,_,_) -> 0.
