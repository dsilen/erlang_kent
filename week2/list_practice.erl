-module(list_practice).
-export([prodD/1,
    prodT/1,
    prodFold/1,
    lmaxD/1,
    lmaxT/1]).

% product
prodD([]) -> 1;
prodD([N|Ns]) -> N*prodD(Ns).

prodT(N) -> prodT(N,1).
prodT([],Prod) -> Prod;
prodT([N|Ns],Prod) -> prodT(Ns,N*Prod).

prodFold(Ns) -> lists:foldl(fun(N,Prod) -> N*Prod end, 1, Ns).



% max
lmaxD([N]) -> N;
lmaxD([N,_|Ns]) -> max(N,lmaxD(Ns)).

lmaxT([N|Ns]) -> lmaxT(Ns,N).
lmaxT([],M) -> M;
lmaxT([N|Ns],M) -> lmaxT(Ns,max(M,N)).
