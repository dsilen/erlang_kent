-module(more).
-export([perm/1, join/2, concat/1]).

-spec perm([T]) -> [[T]].
perm([X]) -> [[X]];
perm([X|Xs]) ->
  Perm = perm(Xs),
  Splits = lists:flatmap(fun(N) -> allSplits(N) end, Perm),
  insertInSplits(X,Splits).

insertInSplits(_,[]) -> [];
insertInSplits(X,[{S1,S2}|Ss]) -> [concat([S1,[X],S2|[]])|insertInSplits(X,Ss)].

allSplits(Xs) -> allSplits(Xs,length(Xs)).
allSplits(Xs,0) -> [lists:split(0,Xs)];
allSplits(Xs,N) -> [lists:split(N,Xs)|allSplits(Xs,N-1)].

-spec concat([[T]]) -> [T].
concat([]) -> [];
concat([X|Xs]) -> join(X,concat(Xs)).

-spec join([T],[T]) -> [T].
join([],Ys) -> Ys;
join([X|Xs],Ys) -> [X|join(Xs,Ys)].
