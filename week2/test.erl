-module(test).
-export([%foo/2,
  %merge/2,
  baz/1,
  x/1]).

x({ok}) -> ok1;
x({ok}) -> ok2.



% merge([],Ys) -> Ys;
% merge(Xs,[]) -> Xs;
% merge([X|Xs],[Y|Ys]) when X<Y ->
%     [ X | merge(Xs,[Y|Ys]) ];
% merge([X|Xs],[Y|Ys]) when X>Y ->
%     [ Y | merge([X|Xs],Ys) ];
% merge([X|Xs],[Y|Ys]) ->
%     [ X | merge(Xs,Ys) ].

% foo(_,[])              -> [];
% foo(Y,[X|_]) when X==Y -> [X];
% foo(Y,[X|Xs])          -> [X | foo(Y,Xs) ].


% bar (N, [Y]) ->
%  	[Y];

% bar (N, [N]) ->
% 	[];

% bar (N, [Y|Ys]) when N =/= Y ->
% 	[Y|bar (N, Ys)];

% bar (N, [Y|Ys]) ->
% 	Ys.



baz([])     -> [];
baz([X|Xs]) -> [X | baz(zab(X,Xs))].

zab(N,[])     -> [];
zab(N,[N|Xs]) -> zab(N,Xs);
zab(N,[X|Xs]) -> [X | zab(N,Xs)].