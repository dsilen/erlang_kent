#!/usr/bin/env escript

main([]) -> 
    {ok, Filenames} = file:list_dir("."),
    [run_eunit(File) || File <- Filenames, string:right(File,10) == "_tests.erl", File =/= "run_tests.erl" ],
    erlang:halt(0).

run_eunit(File) ->
    compile:file(File),
    Module = list_to_atom(filename:basename(File, ".erl")),
    io:format("Testing ~-12s", [Module]),
    eunit:test(Module).
