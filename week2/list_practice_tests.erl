% c(list_practice), c(list_practice_tests), eunit:test(list_practice).

-module(list_practice_tests).
-include_lib("eunit/include/eunit.hrl").

% % helper function
% between(X,Min,Max) -> (X > Min) and (X < Max).


prodD_test() ->
1 = list_practice:prodD([]),
6 = list_practice:prodD([1,2,3]).

prodT_test() ->
1 = list_practice:prodT([]),
6 = list_practice:prodT([1,2,3]).

prodFold_test() ->
1 = list_practice:prodFold([]),
6 = list_practice:prodFold([1,2,3]).

lmaxD_test() ->
1 = list_practice:lmaxD([1]),
3 = list_practice:lmaxD([1,2,3]).

lmaxT_test() ->
1 = list_practice:lmaxT([1]),
3 = list_practice:lmaxT([1,2,3]).
