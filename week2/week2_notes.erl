-module(week2_notes).
-export([circles/1,
        circles2/1]).

% % List pattern matching det är möjligt matcha flera saker innan tail.
% [X,Y,Z | Rest] = [1,2,3,4].

% % case expressions
% case [1] of
%     [] -> 1;
%     _ -> 0
% end.

circles([]) -> [];
circles([{circle,_,_}=C|Xs]) -> [C|circles(Xs)];
circles([_|Xs]) -> circles(Xs).

circles2([]) -> [];
circles2([C|Xs]) ->
    case C of
        {circle,_,_} -> [C|circles2(Xs)];
        _ -> circles2(Xs)
    end.


% % "to think about"
% nub - remove duplicates
% median - middle element
% mode - most common element

