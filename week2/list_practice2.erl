-module(list_practice2).
-export([double/1,
        evens/1,
        median/1,
        mode/1,
        take/2,
        takeT/2,
        nub/1,
        palindrome/1]).

% Define an Erlang function double/1 to double the elements of a list of numbers.
double([]) -> [];
double([N|Ns]) -> [N*2|double(Ns)].

% Define a function evens/1 that extracts the even numbers from a list of integers.
evens([]) -> [];
evens([N|Ns]) when N rem 2 =:= 0 -> [N|evens(Ns)];
evens([_|Ns])-> evens(Ns).

% the median of a list of numbers: this is the middle element
% when the list is ordered (if the list is of even length you
% should average the middle two)
median([]) -> [];
median(Xs) ->
    Sorted = lists:sort(Xs),
    Length = length(Xs),
    case Length rem 2 of
        1 -> hd(lists:nthtail(Length div 2, Sorted));
        0 ->
            [N1,N2|_] = lists:nthtail(Length div 2 - 1, Sorted),
            (N1+N2)/2
    end.

% the modes of a list of numbers: this is a list consisting of the numbers
% that occur most frequently in the list;
% if there is is just one, this will be a list with one element only
mode([]) -> [];
mode(Xs) ->
    Uniq = lists:usort(Xs),
    Counters = lists:map(fun(X) -> {X,0} end,Uniq),
    mode(Xs,Counters).

mode([],Counters) ->
    SaveBiggestFun = fun({N,C},[{_,AC}|_]=A) ->
        case C > AC of
            true -> [{N,C}];
            false -> [{N,C}|A]
        end
    end,
    OrderedC = lists:keysort(2,Counters),
    Biggest = lists:foldl(SaveBiggestFun,[hd(OrderedC)],tl(OrderedC)),
    lists:sort(lists:map(fun({N,_})-> N end, Biggest));
mode([N|Ns],Counters) ->
    {value,{_,C},OtherCs} = lists:keytake(N,1,Counters),
    mode(Ns,[{N,C+1}|OtherCs]).

%take
-spec take(integer(),[T]) -> [T].
take(_,[]) -> [];
take(0,_) -> [];
take(N,[X|Xs]) when N > 0 ->
    [X|take(N-1,Xs)].

-spec takeT(integer(),[T]) -> [T].
takeT(N,Xs) when N >= 0 -> takeT(N,Xs,[]).
takeT(0,_,Acc) -> lists:reverse(Acc);
takeT(_,[],Acc) -> lists:reverse(Acc);
takeT(N,[X|Xs],Acc) -> takeT(N-1,Xs,[X|Acc]).

% nub
contains(_,[]) -> false;
contains(X,[Y|_]) when X =:= Y -> true;
contains(X,[_|Ys]) -> contains(X,Ys).

-spec nub([T]) -> [T].
nub(Xs) -> nub(Xs,[]).
nub([],Acc) -> lists:reverse(Acc);
nub([X|Xs],Acc) ->
    case contains(X,Acc) of
        true -> nub(Xs,Acc);
        false -> nub(Xs,[X|Acc])
    end.

% palindrome
palindrome([]) -> true;
palindrome(Xs) ->
    Ys = removeWhitespaceAndToLower(Xs),
    HalfLength = length(Ys) div 2,
    Front = lists:split(HalfLength,Ys),
    Back = lists:split(HalfLength, lists:reverse(Ys)),
    Front =:= Back.

removeWhitespaceAndToLower([]) -> [];
removeWhitespaceAndToLower([X|Xs]) when X >= 97 andalso X =< 122 -> [X|removeWhitespaceAndToLower(Xs)];
removeWhitespaceAndToLower([X|Xs]) when X >= 65 andalso X =< 90 -> [X+32|removeWhitespaceAndToLower(Xs)];
removeWhitespaceAndToLower([_|Xs]) -> removeWhitespaceAndToLower(Xs).

