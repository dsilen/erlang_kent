% c(list_practice2), c(list_practice2_tests), eunit:test(list_practice2).

-module(list_practice2_tests).
-include_lib("eunit/include/eunit.hrl").


double_test() ->
[2,4,6] = list_practice2:double([1,2,3]),
[] = list_practice2:double([]).

evens_test() ->
[2] = list_practice2:evens([1,2,3]),
[] = list_practice2:evens([]).

mode_test() ->
[] = list_practice2:mode([]),
[1] = list_practice2:mode([1]),
[1,3] = list_practice2:mode([4,5,3,1,1,3,2]).

median_test() ->
1 = list_practice2:median([1]),
1.5 = list_practice2:median([2,1]),
2 = list_practice2:median([2,1,3]),
3 = list_practice2:median([4,2,5,1,3]).

take_test() ->
?assertEqual([],list_practice2:take(0,"hello")),
?assertEqual([],list_practice2:take(1,"")),
?assertEqual("he",list_practice2:take(2,"hello")).

takeT_test() ->
?assertEqual([],list_practice2:takeT(0,"hello")),
?assertEqual([],list_practice2:takeT(1,"")),
?assertEqual("he",list_practice2:takeT(2,"hello")).

nub_test() ->
?assertEqual([],list_practice2:nub([])),
?assertEqual([a],list_practice2:nub([a,a,a])),
?assertEqual([a,b],list_practice2:nub([a,b,b])),
?assertEqual([a,b],list_practice2:nub([a,b,b,a])),
?assertEqual([2,4,1,3],list_practice2:nub([2,4,1,3,3,1])).

palindrome_test() ->
?assertEqual(true,list_practice2:palindrome("")),
?assertEqual(true,list_practice2:palindrome("madam i'm adam.")),
?assertEqual(false,list_practice2:palindrome("madam i'm adamm.")). 
